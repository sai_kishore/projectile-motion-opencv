# Projectile Motion Detection Using Opencv

Overview:
Detects the path of an Yellow ball thrown over a white baground and 
estimates the destination where the ball will land. 

## Installation
```sh 
	pip install -r requirements.txt 
```
## Execution
	1) Use a backdrop which does not contain yellow color in it (Preferably white) 
	2) Use proper lighting and use a yellow coloured ball for testing.
	3) Install the required packages from requirements.txt 
	4) Run main.py 
	5) Throw the ball slowly in any direction and observe how it detects the destination point when the ball is in mid-air 
## Functions Used
### Detecting the yellow ball 
``` python 

lower_yellow = np.array([20, 100, 100])
upper_yellow = np.array([30, 255, 255])
mask = cv2.inRange(hsv, lower_blue, upper_blue)
mask = cv2.inRange(hsv, lower_yellow, upper_yellow)
contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

for c in contours:
	if cv2. b  contourArea(c) > 200:
		#print(cv2.contourArea(c))
		#frame_count+=1
		approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
		cv2.drawContours(frame,[approx],-1,(0,255,0),3)
		(x, y, w, h)= cv2.boundingRect(c)

```
 The above code Draws a green mask over the mentioned colour and displays it. 
Copy paste the same code and change values of the lower and upper hsv to detect multiple colours in the same frame

``` python 

cv2.circle(frame1, (x+int(w//2), y+int(h//2)), 5, (0, 255, 255), -1)
centre_x = x+(w//2)
centre_y = y+(h//2)

lst.append([centre_x,centre_y])
```
The above code draws a yellow circle on the contour detected and logs the centres detected on every frame. These contour centers are used to determine the Quadratic Equation 

### Finding the parabola 
Extracting the points from contours

``` python 
lst.append([centre_x,centre_y])
all_points.append([centre_x,centre_y])
if len(lst) > 2:
    lst.pop(0)
    if lst[0][1] < lst[1][1] and not max_found: 
        if not para_taken:
            para_1 = all_points[-1]
            para_2 = all_points[-2]
            para_3 = all_points[-3]
            # para_points = [para_1,para_2,para_3]
            para_taken = True
```

   para_1 : The previous contour from the maximum contour of the parabola detected.
	para_2 : The maximum detected contour of the trajectory
	para_3 : The next contour to the maximum contour of the trajectory
	Using these three points and the parabolic equation the destination of the 
	Trajectory on any plane can be computed 
	Parabola equation: y = ax^2 + bx + c 
	
``` python 

	def calc_parabola_vertex(x1, y1, x2, y2, x3, y3):
	'''
	args : x1,y1,x2,y2,x3,y3
	returns: a,b,c
	'''
	denom = (x1-x2) * (x1-x3) * (x2-x3);
	A     = (x3 * (y2-y1) + x2 * (y1-y3) + x1 * (y3-y2)) / denom;
	B     = (x3*x3 * (y1-y2) + x2*x2 * (y3-y1) + x1*x1 * (y2-y3)) / denom;
	C     = (x2 * x3 * (x2-x3) * y1+x3 * x1 * (x3-x1) * y2+x1 * x2 * (x1-x2) * y3) / denom;

	return A,B,C
 ```

 	The above Function calculates and returns the values of the Coefficients of the Parabola Equation. Using these coefficients, the destination of the projectile is estimated.

``` python 

def solve_Quadratic(a,b,c,y):
	'''
	args: a,b,c,y
	returns: sol1,sol2'''

    c = c -y
    d = (b**2) - (4*a*c)
    # find two solutions
    if d<0:
        sol1 = (-b-cmath.sqrt(d))/(2*a)
        sol2 = (-b+cmath.sqrt(d))/(2*a)
    if d>=0:
        sol1 = (-b-math.sqrt(d))/(2*a)
        sol2 = (-b+math.sqrt(d))/(2*a)
        print ('Solution',sol1,sol2)
    return (sol1,sol2)
```

The above function uses the coefficients to compute the X value of the destination.
	The y value of the destination is constant as the slider moves in only one direction.

 