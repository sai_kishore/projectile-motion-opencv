import cv2
import numpy as np
import time
import math
import random
# import pyautogui
import cmath

#from PIL import ImageGrab


#bbox = (0, 0, 720, 600)

def nothing(x):
    pass

##def capture():
##    screenshot = pyautogui.screenshot()
##    screenshot.save("/home/murali/projects/interns/ball_catch/data/screen{}.png".format(inc))
    
def angle3pt(a, b, c):
    ang = math.degrees(math.atan2(c[1]-b[1], c[0]-b[0]) - math.atan2(a[1]-b[1], a[0]-b[0]))
    
    if ang<0:
        return abs(ang + 360)
    else:
        return abs(ang)

def calc_parabola_vertex(x1, y1, x2, y2, x3, y3):
	'''
	Adapted and modifed to get the unknowns for defining a parabola:
	http://stackoverflow.com/questions/717762/how-to-calculate-the-vertex-of-a-parabola-given-three-points
	'''

	denom = (x1-x2) * (x1-x3) * (x2-x3);
	A     = (x3 * (y2-y1) + x2 * (y1-y3) + x1 * (y3-y2)) / denom;
	B     = (x3*x3 * (y1-y2) + x2*x2 * (y3-y1) + x1*x1 * (y2-y3)) / denom;
	C     = (x2 * x3 * (x2-x3) * y1+x3 * x1 * (x3-x1) * y2+x1 * x2 * (x1-x2) * y3) / denom;

	return A,B,C

def solve_Quadratic(a,b,c,y):

    c = c -y
    d = (b**2) - (4*a*c)
    # find two solutions
    if d<0:
        sol1 = (-b-cmath.sqrt(d))/(2*a)
        sol2 = (-b+cmath.sqrt(d))/(2*a)
    if d>=0:
        sol1 = (-b-math.sqrt(d))/(2*a)
        sol2 = (-b+math.sqrt(d))/(2*a)
        print ('Solution',sol1,sol2)
    return (sol1,sol2)

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_AUTOFOCUS, 0) 

#cv2.namedWindow("Trackbars")


##cv2.createTrackbar("L - H", "Trackbars", 0, 179, nothing)
##cv2.createTrackbar("L - S", "Trackbars", 0, 255, nothing)
##cv2.createTrackbar("L - V", "Trackbars", 0, 255, nothing)
##cv2.createTrackbar("U - H", "Trackbars", 179, 179, nothing)
##cv2.createTrackbar("U - S", "Trackbars", 255, 255, nothing)
##cv2.createTrackbar("U - V", "Trackbars", 255, 255, nothing)

cord_list = []
obj_seen = False
initial_cord = []
max_cord = []
mid_cord = []
all_points = []

maxroot = False
# minroot = False
# 
# screen resolution
screen_width = 640
screen_height  = 480

a = 20

#frame_count = 0
first_active = False
second_active = False

cord = []
font = cv2.FONT_HERSHEY_SIMPLEX

#pix = 100/640# 1 pixel equals to this much centimeter

draw = False

lst = []
#present = []

#present_flag = False
#prev_flag = False

max_point = []
max_found_disp = False
max_found = False
first_taken = False
inc = 1

para_taken = False
para_points = []
para_1 = []
para_2 = []
para_3 = []
angle = 0.0
a1 = (0,0)
a2 = (0,0)
a3 = (0,0)
angle_line_point = 0


while True:
    

    _, frame = cap.read()
    #frame = cv2.resize(frame,(screen_width,screen_height))
    #frame = cv2.resize(frame, (width,height))
    if not first_taken:
        frame1 = frame.copy()
        first_taken = True
        
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

##    l_h = cv2.getTrackbarPos("L - H", "Trackbars")
##    l_s = cv2.getTrackbarPos("L - S", "Trackbars")
##    l_v = cv2.getTrackbarPos("L - V", "Trackbars")
##    u_h = cv2.getTrackbarPos("U - H", "Trackbars")
##    u_s = cv2.getTrackbarPos("U - S", "Trackbars")
##    u_v = cv2.getTrackbarPos("U - V", "Trackbars")


    #lower_blue = np.array([l_h, l_s, l_v])
    #upper_blue = np.array([u_h, u_s, u_v])
    #lower_blue = np.array([38, 86, 0])
    #upper_blue = np.array([121, 255, 255])
    lower_yellow = np.array([20, 100, 100])
    upper_yellow = np.array([30, 255, 255])
    #mask = cv2.inRange(hsv, lower_blue, upper_blue)
    mask = cv2.inRange(hsv, lower_yellow, upper_yellow)
    contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

    #grid for mesureing object flow and speed
##    for i in range(15):
##        cv2.line(frame, (0+inc,0), (0+inc, 480), (0, 0, 255), 1)#1st from x zero
##        inc+=20
##        
    cv2.line(frame, (0,screen_height-15), (screen_width, screen_height-15), (random.randint(0,155), 0, 0), 2)
    cv2.line(frame1, (0,screen_height-15), (screen_width, screen_height-15), (random.randint(0,155), 0, 0), 2)



##    if not found:
##        for c in contours:
##        if cv2.contourArea(c) > 200:
##            #print(cv2.contourArea(c))
##            #frame_count+=1
##            approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
##            cv2.drawContours(frame,[approx],-1,(0,255,0),3)
##            (x, y, w, h)= cv2.boundingRect(c)
##            bbox = (x,y,w,h)
##            tracker = cv2.TrackerBoosting_create()
##            ok = tracker.init(frame, bbox)
##            
##        ok, bbox = tracker.update(frame)
##
##            p1 = (int(bbox[0]), int(bbox[1]))
##            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
##            cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)
            

    for c in contours:
        if cv2.contourArea(c) > 200:
            #print(cv2.contourArea(c))
            #frame_count+=1
            approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
            cv2.drawContours(frame,[approx],-1,(0,255,0),3)
            (x, y, w, h)= cv2.boundingRect(c)
            
            
            if not obj_seen:
                
##                if (initial_cord[1] < 380):
##                    y_diff = 380 - initial_cord[1]
##                    
                
                
                
                start_time = time.time()
                print('obj seen')
                initial_cord = [x+(w//2),y+(h//2)]
                obj_seen = True
                first_active = True
               
                    

            
            #cv2.rectangle(frame,(x-20,y-20),(x+100,y+100), (0,0, 255), 2)
            # cv2.circle(frame, (x+int(w//2), y+int(h//2)), 7, (0, 0, 255), -1)
            cv2.circle(frame1, (x+int(w//2), y+int(h//2)), 5, (0, 255, 255), -1)
            centre_x = x+(w//2)
            centre_y = y+(h//2)

            lst.append([centre_x,centre_y])
            all_points.append([centre_x,centre_y])
            if len(lst) > 2:
                lst.pop(0)
                if lst[0][1] < lst[1][1] and not max_found:
                    if not para_taken:
                        para_1 = lst[1]
                        para_2 = lst[0]
                        para_3 = all_points[-3]
                        # para_points = [para_1,para_2,para_3]
                        para_taken = True
                        y_lim = 480
                        y_new = 465
                        x_new1,y_new1 = para_3
                        x_new2,y_new2 = para_2
                        x_new3,y_new3 = para_1


                        x1,y1=[para_3[0],y_new-para_3[1]]
                        x2,y2=[para_2[0],y_new-para_2[1]]                        
                        x3,y3=[para_1[0],y_new-para_1[1]]

                        #print("Para inside", para_1,para_2,para_3)



                        a,b,c=calc_parabola_vertex(x1, y1, x2, y2, x3, y3)

                        coord_new1,coord_new2= solve_Quadratic(a,b,c,y_lim-y_new)
                        #print (coord_new1,coord_new2)
                        # print(int(x_new1.real),int(x_new2.real))
                        x4,y4 = [int(coord_new1.real),y_new]
                        x5,y5 = [int(coord_new2.real),y_new]
                        
                        #print('Sol1:', (x4,y4),'Sol2:',(x5,y5))

                        #Calculate the unknowns of the equation y=ax^2+bx+c
                        #a,b,c=calc_parabola_vertex(para_1[0],para_1[1], para_2[0], para_2[1], para_3[0], para_3[1])

    
                    max_point = lst[0]
                    # cv2.circle(frame, (max_point[0],max_point[1]), 7, (0, random.randint(0,255), 0), -1)
                    # cv2.circle(frame1, (max_point[0],max_point[1]), 7, (0, random.randint(0,255), 0), -1)

                    cv2.circle(frame1, (para_3[0],para_3[1]), 10, (0, 0, 255), -1)
                    cv2.circle(frame1, (para_2[0],para_2[1]), 10, (0, 255, 0), -1)
                    cv2.circle(frame1, (para_1[0],para_1[1]), 10, (255, 0, 0), -1)

                    # print((x_new1,y_new1),(x_new2,y_new2),(x_new3,y_new3))
                    #print("max:",max_point)

                    if x_new1>x_new3:
                        maxroot = True
                        #finding angle if thrown from min to max on x axis
                        a1 = (initial_cord[0]-100, initial_cord[1])
                        a2 = (initial_cord)
                        a3 = (max_point)
                        angle = angle3pt(a3,a2,a1)
                        angle_line_point = 0
                    else:
                        #finding angle if thrown from max to min on x axis
                        a1 = (initial_cord[0]+100, initial_cord[1])
                        a2 = (initial_cord)
                        a3 = (max_point)
                        angle = angle3pt(a3,a2,a1)
                        angle_line_point = screen_width


                    
                    max_found_disp = True
                    max_found = True
                    
            if centre_y < (screen_height-15) and not draw:
                draw = True
                second_active = True
                cord = [centre_x,centre_y]
                end_time = time.time()
            
    #displays first object capture location
    if first_active:
        cv2.rectangle(frame,(initial_cord[0],initial_cord[1]),(initial_cord[0]+50,initial_cord[1]+50), (255,0, 0), 2)

    #displays second object capture location
    if second_active:
        cv2.rectangle(frame,(cord[0],cord[1]),(cord[0]+50,cord[1]+50), (0,255, 0), 2)
        cv2.line(frame, (tuple(cord)), (tuple(initial_cord)), (0, 0, 255), 2)
        cv2.line(frame, (initial_cord[0], initial_cord[1]), (angle_line_point,initial_cord[1]), (0, 0, 255), 2)

        print('initial_cord ++: ',a1)
        print('initial_cord : ',a2)

        print('max_cord: ',a3)

        #reverse_angle = 180 - angle

        #myradians = math.atan2(initial_cord[1]-cord[1], initial_cord[0]-cord[0])
        #mydegrees = math.degrees(myradians)
        #print(angle)
        cv2.putText(frame, 'tilt angle:'+str(round(angle,2)),(20, 20), font,0.6, (0, 0, 0), 2)#angle
        
        # cv2.putText(frame, 'second_cordinate for tilt and velocity',(20, 60), font,0.6, (0, 255, 0), 2)
    
        #elapsed_time = end_time - start_time

        #dist = int(math.sqrt((initial_cord[0] - cord[0]) ** 2 + (initial_cord[1] - cord[1]) ** 2))
        #print('pix_dist',dist)

        if max_found_disp:
            #initial_max_dist = int(math.sqrt((initial_cord[0] - max_point[0]) ** 2 + (initial_cord[1] - max_point[1]) ** 2))

            cv2.line(frame, (initial_cord[0], initial_cord[1]), (max_point[0],max_point[1]), (0, 0, 255), 2)

            #mid_point_x = max_point[0]-initial_cord[0]

            #dest_point = int(mid_point_x*2)

            #cv2.rectangle(frame,(initial_cord[0]+dest_point-15, initial_cord[1]-15),(initial_cord[0]+dest_point+15,initial_cord[1]), (255,255, 255), 4)
            
            # cv2.rectangle(frame,(initial_cord[0]+dest_point-15, 465),(initial_cord[0]+dest_point+15,480), (255,255, 255), 4)
            #cv2.circle(frame, (dest_point, initial_cord[1]-15), 10, (0, random.randint(0,255), 0), -1)
            if maxroot:
                cv2.circle(frame, (x5,y5), 10, (0, random.randint(0,255), 255), -1)
                cv2.circle(frame1, (x5,y5), 10, (0, random.randint(0,255), 255), -1)

                if x5>screen_width:
                    cv2.putText(frame, 'Limit Exceeded',(20, 40), font,0.6, (255, 0, 0), 2)

            else:
                if x4<0:
                    cv2.putText(frame, 'Limit Exceeded',(20, 40), font,0.6, (255, 0, 0), 2)

                cv2.circle(frame, (x4,y4), 10, (0, random.randint(0,255), 255), -1)
                cv2.circle(frame1, (x4,y4), 10, (0, random.randint(0,255), 255), -1)

        #dist = dist*pix
        #print('distance:',dist)
        #print('elapseed',elapsed_time)
        #velocity = dist/(elapsed_time)
        #print('velocity',velocity)
        #print('speed per ', velocity)
        #cv2.putText(frame, 'velocity:'+str(round(velocity,2)),(20, 80), font,0.6, (0, 0, 0), 2)


        #max_height = ((velocity**2)*((math.sin(angle))**2))/(2*9.8)
        #Range = ((velocity**2)*(math.sin(math.radians(2*angle))))/(21.8)
        #print(math.sin(2*angle))

        #print(max_height, Range)
        #max_pix_height = int(max_height*600)*2
        #mid_point_pix = int((Range/2)*600)
        #pix_range = int(Range * 6.40)
        #print('range',Range)
        #print('pix_range',pix_range)
        #print('pix_range',pix_range)
        #cv2.circle(frame, (initial_cord[0]+pix_range,initial_cord[1] ), 15, (0, 0, random.randint(10,255)), -1)

        #cv2.line(frame, (initial_cord[0]+mid_point_pix,initial_cord[1]), (initial_cord[0]+mid_point_pix,initial_cord[1]-max_pix_height), (255, 0, 0), 2)

        #cv2.line(frame, (initial_cord[0],initial_cord[1]-max_pix_height), (500,initial_cord[1]-max_pix_height), (255, 0, 0), 2)
        
        

    if cv2.waitKey(33) & 0xFF == ord('n'):
        print('n pressed')
        ## capture screen
        
        #screenshot = pyautogui.screenshot()
        #screenshot.save("/home/murali/Documents/projects/interns/ball_catch/data/screen{}.png".format(inc))
        #screenshot.save("screen.png")

                
        #file = open("/home/murali/Documents/projects/interns/ball_catch/data/cordinates{}.txt".format(inc),"r+")
 
        #file.write(str(all_points)) 
        
        #file.close() 
        #inc+=1
        
        max_found_disp = False
        first_active = False
        second_active = False
        obj_seen = False
        lst.clear()
        draw = False
        first_taken = False
        max_found = False
        maxroot = False
        para_1.clear()
        para_2.clear()
        para_3.clear()
        para_taken = False
        # minroot = False
        all_points.clear()
        #print("para_1",para_1,para_2,para_3)



    #kernel = np.ones((3,3), np.uint8) 
    #dst = cv2.GaussianBlur(mask,(3,3),cv2.BORDER_DEFAULT)
    #img_erosion = cv2.erode(mask, kernel, iterations=1) 
    #th, dst = cv2.threshold(mask, 0, l_h, cv2.THRESH_BINARY);

    #result = cv2.bitwise_and(frame, frame, mask=mask)

    #cv2.imshow("frame", frame)
    #cv2.imshow("mask", mask)
    
    
    cv2.imshow("frame1", frame1)
    cv2.imshow("frame", frame)
    cv2.moveWindow("frame", 0, 70)
    cv2.moveWindow("frame1", 700, 70)
    #cv2.imshow("result", result)
    
    #print(1/(end_time - start_time))
    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()
