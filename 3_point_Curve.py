import math
import numpy as np 
import cmath 
import math

def calc_parabola_vertex(x1, y1, x2, y2, x3, y3):
	'''
	Adapted and modifed to get the unknowns for defining a parabola:
	http://stackoverflow.com/questions/717762/how-to-calculate-the-vertex-of-a-parabola-given-three-points
	'''

	denom = (x1-x2) * (x1-x3) * (x2-x3);
	A     = (x3 * (y2-y1) + x2 * (y1-y3) + x1 * (y3-y2)) / denom;
	B     = (x3*x3 * (y1-y2) + x2*x2 * (y3-y1) + x1*x1 * (y2-y3)) / denom;
	C     = (x2 * x3 * (x2-x3) * y1+x3 * x1 * (x3-x1) * y2+x1 * x2 * (x1-x2) * y3) / denom;

	return A,B,C

def Parabola_eqn(x1,y1,x2,y2,x3,y3):
	A1 = -(x1**2) + x2**2
	B1 = -x1 + x2
	D1 = -y1 + y2
	A2 =  -(x2)**3 + x3**2
	B2 = -x2 + x3 
	D2 = -y2 + y3
	Bmul = -(B2/B1)
	A3 = Bmul*A1 + A2
	D3 = Bmul*D1 + D2 

	a = D3/A3
	b = (D1- (A1*a)) / B1
	c = y1 - a*(x1**2) - b*(x1)
	return a,b,c

def solve_Quadratic(a,b,c,y):

	c = c -y
	d = (b**2) - (4*a*c)
	# find two solutions
	if d<0:
		sol1 = (-b-cmath.sqrt(d))/(2*a)
		sol2 = (-b+cmath.sqrt(d))/(2*a)
	if d>=0:
		sol1 = (-b-math.sqrt(d))/(2*a)
		sol2 = (-b+math.sqrt(d))/(2*a)
	return (sol1,sol2)

new_y = 465
y_lim = 480

x1,y1=[207,new_y-145]
x2,y2=[290,new_y-110]
x3,y3=[373,new_y-125] 

x1,y1=[373,new_y-151]
x2,y2=[391,new_y-145]
x3,y3=[426,new_y-147] 

# x1,y1=[400,new_y-137]
# x2,y2=[444,new_y-136]
# x3,y3=[482,new_y-164]

# x1,y1=[396,new_y-239]
# x2,y2=[442,new_y-217]
# x3,y3=[484,new_y-220]






#Calculate the unknowns of the equation y=ax^2+bx+c
# a,b,c=calc_parabola_vertex(x1, y1, x2, y2, x3, y3)
a,b,c = calc_parabola_vertex(x1,y1,x2,y2,x3,y3)

# print("Abc",a,b,c)
# print("Abc1",a1,b1,c1)

#Suppose y = 440
x_new1,x_new2= solve_Quadratic(a,b,c,y_lim-new_y)
print(x_new1,x_new2)
# print(int(x_new1.real),int(x_new2.real))

x4,y4 = [int(x_new1.real),new_y]
x5,y5 = [int(x_new2.real),new_y]

print('a,b,c',a,b,c)


#Define x range for which to calc parabola

x_pos=np.arange(0,640,1)
y_pos=[]

#Calculate y values 
for x in range(len(x_pos)):
	x_val=x_pos[x]
	y=(a*(x_val**2))+(b*x_val)+c
	y_pos.append(y)
	# print(x,y)

import matplotlib.pyplot as plt

plt.plot(x_pos, y_pos, linestyle='-.', color='black') # parabola line
plt.scatter(x_pos, y_pos, color='gray') # parabola points
plt.scatter(x4,15,color='k',marker='D',s=70)
plt.scatter(x5,15,color='m',marker='D',s=70)
plt.scatter(x1,y1,color='r',marker="D",s=50) # 1st known xy
plt.scatter(x2,y2,color='g',marker="D",s=50) # 2nd known xy
plt.scatter(x3,y3,color='k',marker="D",s=50) # 3rd known xy
plt.show()