# Projectile Motion Detection Using Opencv

Overview:
Detects the path of an Yellow ball thrown over a white baground and 
estimates the destination where the ball will land. After estimating the 
Landing position it will move the Red Box to that position 

## Installation
```sh 
	pip install -r requirements.txt 
```
## Execution
	1) Use a backdrop which does not contain yellow color in it (Preferably white) 
	2) Use proper lighting and use a yellow coloured ball for testing.
	3) Install the required packages from requirements.txt 
	4) Run main.py 
	5) Throw the ball slowly in any direction and observe how it detects the destination point when the ball is in mid-air

## Hardware Requirements
1) Red coloured Box/ Cup 
2) Linear Rail set up 
3) Arduino Uno 
4) CNC Shield
5) 12V smps input 
6) Stepper motor ( NEMA17 )


## Functions Used
### Detecting the yellow ball 
``` python 

lower_yellow = np.array([20, 100, 100])
upper_yellow = np.array([30, 255, 255])
mask = cv2.inRange(hsv, lower_blue, upper_blue)
mask = cv2.inRange(hsv, lower_yellow, upper_yellow)
contours, hierarchy = cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

for c in contours:
	if cv2. b  contourArea(c) > 200:
		#print(cv2.contourArea(c))
		#frame_count+=1
		approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
		cv2.drawContours(frame,[approx],-1,(0,255,0),3)
		(x, y, w, h)= cv2.boundingRect(c)

```
 The above code Draws a green mask over the mentioned colour and displays it. 
Copy paste the same code and change values of the lower and upper hsv to detect multiple colours in the same frame

``` python 

cv2.circle(frame1, (x+int(w//2), y+int(h//2)), 5, (0, 255, 255), -1)
centre_x = x+(w//2)
centre_y = y+(h//2)

lst.append([centre_x,centre_y])
```
The above code draws a yellow circle on the contour detected and logs the centres detected on every frame. These contour centers are used to determine the Quadratic Equation 

### Finding the parabola 
Extracting the points from contours

``` python 
lst.append([centre_x,centre_y])
all_points.append([centre_x,centre_y])
if len(lst) > 2:
    lst.pop(0)
    if lst[0][1] < lst[1][1] and not max_found: 
        if not para_taken:
            para_1 = all_points[-1]
            para_2 = all_points[-2]
            para_3 = all_points[-3]
            # para_points = [para_1,para_2,para_3]
            para_taken = True
```

   para_1 : The previous contour from the maximum contour of the parabola detected.
	para_2 : The maximum detected contour of the trajectory
	para_3 : The next contour to the maximum contour of the trajectory
	Using these three points and the parabolic equation the destination of the 
	Trajectory on any plane can be computed 
	Parabola equation: y = ax^2 + bx + c 
	
``` python 

	def calc_parabola_vertex(x1, y1, x2, y2, x3, y3):
	'''
	args : x1,y1,x2,y2,x3,y3
	returns: a,b,c
	'''
	denom = (x1-x2) * (x1-x3) * (x2-x3);
	A     = (x3 * (y2-y1) + x2 * (y1-y3) + x1 * (y3-y2)) / denom;
	B     = (x3*x3 * (y1-y2) + x2*x2 * (y3-y1) + x1*x1 * (y2-y3)) / denom;
	C     = (x2 * x3 * (x2-x3) * y1+x3 * x1 * (x3-x1) * y2+x1 * x2 * (x1-x2) * y3) / denom;

	return A,B,C
 ```

 	The above Function calculates and returns the values of the Coefficients of the Parabola Equation. Using these coefficients, the destination of the projectile is estimated.

``` python 

def solve_Quadratic(a,b,c,y):
	'''
	args: a,b,c,y
	returns: sol1,sol2'''

    c = c -y
    d = (b**2) - (4*a*c)
    # find two solutions
    if d<0:
        sol1 = (-b-cmath.sqrt(d))/(2*a)
        sol2 = (-b+cmath.sqrt(d))/(2*a)
    if d>=0:
        sol1 = (-b-math.sqrt(d))/(2*a)
        sol2 = (-b+math.sqrt(d))/(2*a)
        print ('Solution',sol1,sol2)
    return (sol1,sol2)
```

The above function uses the coefficients to compute the X value of the destination.
The y value of the destination is constant as the slider moves in only one direction.


## Setting up the Catching box (Red Colour)

### Finding the Box's Center
``` python 
	lower_red=np.array([0,100,100])
    higher_red=np.array([10,255,255])
    mask1_red=cv2.inRange(hsv,lower_red,higher_red)
    lower_red=np.array([160,100,100])
    higher_red=np.array([179,255,255])
    mask2_red=cv2.inRange(hsv,lower_red,higher_red)
    mask_red=mask1_red+mask2_red
    kernel=np.ones((3,3),np.uint8)
    #mask_red=cv2.erode(mask_red,kernel)
    mask_red = cv2.dilate(mask_red, kernel, iterations=1) 
    contours_red, hierarchy = cv2.findContours(mask_red,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

    for c in contours_red:
        if cv2.contourArea(c) > 500:
            approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
            cv2.drawContours(frame,[approx],-1,(0,255,0),3)
            (x_r, y_r, w_r, h_r)= cv2.boundingRect(c)
            centre_x_r = x_r+(w_r//2)
            centre_y_r = y_r+(h_r//2)
            cv2.circle(frame, (centre_x_r, centre_y_r), 7, (255, 0, 255), -1)
```  

The Above code finds the contours of the red mask and gives the center coordinates of the Red Box 


### Stepper motor 

``` python 

from utils.grbl_utils import *
from utils.time_utils import *

grbl_board = open_grbl_device({'port': '/dev/ttyACM0', 'baud_rate': 115200})

def send_gcode(board, gcode):
    if gcode.isspace() is False and len(gcode) > 0:
        logging.debug('Sending G-Code: ' + gcode)
        board.write((gcode + '\n').encode())
        grbl_out = board.readline()
        print(gcode + ' : ' + str(grbl_out.strip()))

Full_rail = 11.58
speed = 900

send_gcode(grbl_board, 'G21 G91 G1 X{} F{}'.format(Full_rail,speed))
```  

The above code slides the box attached to the linear rail from the leftmost end to the right most end. 
The stepper motor value that is required to move the box over the length of the rail is 11.58 

Note: 
For clockwise rotation Full_rail = -Full_rail
For AntiClockwise Roration Full_rail = Full_rail


### Marking the boundaries of the Rail in pixels 

``` python 

if cv2.waitKey(33) & 0xFF == ord('a'):
    far_step = centre_x_r
    print("far step value taken",far_step)
    print("Screen widhth - a ",screen_width-far_step)

if cv2.waitKey(33) & 0xFF == ord('b'):
    near_step = centre_x_r
    print("Near Step value taken")
    step_val = 11.58 / (near_step-far_step)
    print ("Stepval",step_val)

``` 

Pressing the key 'a' will note down the pixel position of the box at the right end of the rail ( Away from stepper)

Pressing the key 'b' will note down the pixel position of the box at the left end( Near stepper)

Note: Initially keep the box at right end, press 'a', then manually move the box to left end , press 'b'

After finding the far_step and near_step values do not change your camera position or the box position 

The Step val tells us how much the stepper motor should rotate to cover one pixel. 
Usually Its in the range of 0.049 to 0.052


### Moving the box to the Final location using the linear rail 

``` python 
send_gcode(grbl_board, '$X')
                        
    if centre_x_r < x4:
        print('cup is to left of destination')

        temp1 = centre_x_r

        while temp1 < x4:
            if temp1 > near_step-5:
                break
            send_gcode(grbl_board, 'G21 G91 G1 X{} F900'.format(step_val))
            temp1+=1
    
    if centre_x_r > x4:
        print('cup is to right of destination')
        temp2 = centre_x_r

        while temp2 > x4:
            if temp2 < far_step-5:
                break
            send_gcode(grbl_board, 'G21 G91 G1 X-{} F900'.format(step_val))
            temp2-=1
``` 
As we know using the parabola equation two roots are found x4,x5
temp1 is the current position of box 
The pixel distance of Temp1 is adjusted so that it reaches the destination 
With every change in the pixel distance, the stepper motor values are also incremented/ decremented 
to match the position of the destination with that of the box 