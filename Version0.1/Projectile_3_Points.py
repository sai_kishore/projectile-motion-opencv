## projectile motion estimation using parabola formula==(version 1)
## Tested with yellow ball(vary the mask to use other coloured objects)
## press 'n' to clear the drawings after throwing the ball. It clears all values stored till now
## laptop webcam is used to reduce the computational load
## worked by Kishore and Murali -- dated 29 Nov 2019

import cv2
import numpy as np
import time
import math
import random
import cmath
import serial
import _thread
from utils.grbl_utils import *
from utils.time_utils import *
import numpy as np

grbl_board = open_grbl_device({'port': '/dev/ttyACM0', 'baud_rate': 115200})
# Capture the input frame from webcam
def send_gcode(board, gcode):
    if gcode.isspace() is False and len(gcode) > 0:
        logging.debug('Sending G-Code: ' + gcode)
        board.write((gcode + '\n').encode())
        grbl_out = board.readline()
        print(gcode + ' : ' + str(grbl_out.strip()))

dis_travel=0

def nothing(x):
    pass

def angle3pt(a, b, c):
    ang = math.degrees(math.atan2(c[1]-b[1], c[0]-b[0]) - math.atan2(a[1]-b[1], a[0]-b[0]))
    
    if ang<0:
        return abs(ang + 360)
    else:
        return abs(ang)

def calc_parabola_vertex(x1, y1, x2, y2, x3, y3):
    try:
        denom = (x1-x2) * (x1-x3) * (x2-x3);
        A     = (x3 * (y2-y1) + x2 * (y1-y3) + x1 * (y3-y2)) / denom;
        B     = (x3*x3 * (y1-y2) + x2*x2 * (y3-y1) + x1*x1 * (y2-y3)) / denom;
        C     = (x2 * x3 * (x2-x3) * y1+x3 * x1 * (x3-x1) * y2+x1 * x2 * (x1-x2) * y3) / denom;
        return A,B,C
    except:
        pass

def solve_Quadratic(a,b,c,y):
    try:
        c = c -y
        d = (b**2) - (4*a*c)
        # find two solutions
        if d<0:
            sol1 = (-b-cmath.sqrt(d))/(2*a)
            sol2 = (-b+cmath.sqrt(d))/(2*a)
        if d>=0:
            sol1 = (-b-math.sqrt(d))/(2*a)
            sol2 = (-b+math.sqrt(d))/(2*a)
            
        return (sol1,sol2)
    
    except Exception as e: print(e)

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_AUTOFOCUS, 0) 

cord_list = []
obj_seen = False
initial_cord = []
max_cord = []
mid_cord = []
all_points = []

maxroot = False

# screen resolution
screen_width = 640
screen_height  = 480

a = 20

near_step = 0
far_step = 0
first_active = False
second_active = False

cord = []
font = cv2.FONT_HERSHEY_SIMPLEX

draw = False

lst = []

max_point = []
max_found_disp = False
max_found = False
first_taken = False
inc = 1

para_taken = False
para_points = []
para_1 = []
para_2 = []
para_3 = []
angle = 0.0
a1 = (0,0)
a2 = (0,0)
a3 = (0,0)
angle_line_point = 0

cup_pos = []

temp3_taken = False
temp1_taken = False
temp2_taken = False

cv2.namedWindow('binary_slider')

# create trackbars
cv2.createTrackbar('lh','binary_slider',23,255,nothing)
cv2.createTrackbar('ls','binary_slider',73,255,nothing)
cv2.createTrackbar('lv','binary_slider',127,255,nothing)
cv2.createTrackbar('uh','binary_slider',119,255,nothing)
cv2.createTrackbar('us','binary_slider',255,255,nothing)
cv2.createTrackbar('uv','binary_slider',255,255,nothing)


while True:
    try:
        _, frame = cap.read()
        #frame = cv2.resize(frame,(screen_width,screen_height))
        if not first_taken:
            frame1 = frame.copy()
            first_taken = True
            
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        #mask for yellow
        lh = cv2.getTrackbarPos('lh','binary_slider')
        ls= cv2.getTrackbarPos('ls','binary_slider')
        lv = cv2.getTrackbarPos('lv','binary_slider')
        uh = cv2.getTrackbarPos('uh','binary_slider')
        us = cv2.getTrackbarPos('us','binary_slider')
        uv = cv2.getTrackbarPos('uv','binary_slider')

        #lower_yellow = np.array([20, 100, 100])
        #upper_yellow = np.array([30, 255, 255])

        lower_yellow = np.array([lh, ls, lv])
        upper_yellow = np.array([uh, us, uv])

        # lower_red = np.array([161,155,84])
        # upper_red = np.array([179,255,255])

        #lower_blue=np.array([90,130,130])
        #higher_blue=np.array([120,255,255])
        #mask_blue=cv2.inRange(hsv,lower_blue,higher_blue)

        lower_red=np.array([0,100,100])
        higher_red=np.array([10,255,255])
        mask1_red=cv2.inRange(hsv,lower_red,higher_red)
        lower_red=np.array([160,100,100])
        higher_red=np.array([179,255,255])
        mask2_red=cv2.inRange(hsv,lower_red,higher_red)
        mask_red=mask1_red+mask2_red
        kernel=np.ones((3,3),np.uint8)
        #mask_red=cv2.erode(mask_red,kernel)
        mask_red = cv2.dilate(mask_red, kernel, iterations=1) 
        
        mask_yellow = cv2.inRange(hsv, lower_yellow, upper_yellow)
        mask_yellow = cv2.dilate(mask_yellow, kernel, iterations=1) 
        # mask_red = cv2.inRange(hsv, lower_red, upper_red)
        
        #contours_blue, hierarchy = cv2.findContours(mask_blue,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

        contours_yellow, hierarchy = cv2.findContours(mask_yellow,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        contours_red, hierarchy = cv2.findContours(mask_red,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

        cv2.line(frame, (0,screen_height-15), (screen_width, screen_height-15), (random.randint(0,155), 0, 0), 2)
        cv2.line(frame1, (0,screen_height-15), (screen_width, screen_height-15), (random.randint(0,155), 0, 0), 2)
        #ref line for red contour
        cv2.line(frame, (0,screen_height-60), (screen_width, screen_height-60), (random.randint(0,155), 0, 0), 2)

        cv2.line(frame, (far_step,0), (far_step, screen_height), (0, 0, 0), 2)
        cv2.line(frame, (near_step,0), (near_step, screen_height), (0, 0, 0), 2)
        


        for c in contours_red:
            if cv2.contourArea(c) > 500:
                approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
                cv2.drawContours(frame,[approx],-1,(0,255,0),3)
                (x_r, y_r, w_r, h_r)= cv2.boundingRect(c)
                centre_x_r = x_r+(w_r//2)
                centre_y_r = y_r+(h_r//2)
                cv2.circle(frame, (centre_x_r, centre_y_r), 7, (255, 0, 255), -1)


        for c in contours_yellow:
            if cv2.contourArea(c) > 200:
                approx = cv2.approxPolyDP(c, 0.009 * cv2.arcLength(c, True), True)
                cv2.drawContours(frame,[approx],-1,(0,255,0),3)
                (x, y, w, h)= cv2.boundingRect(c)
                
                
                if not obj_seen:                
                    start_time = time.time()
                    print('obj seen')
                    initial_cord = [x+(w//2),y+(h//2)]
                    obj_seen = True
                    first_active = True
                            
                cv2.circle(frame1, (x+int(w//2), y+int(h//2)), 5, (0, 255, 255), -1)
                centre_x = x+(w//2)
                centre_y = y+(h//2)

                lst.append([centre_x,centre_y])
                all_points.append([centre_x,centre_y])
                if len(lst) > 2:
                    lst.pop(0)
                    if lst[0][1] < lst[1][1] and not max_found:
                        if not para_taken:
                            para_1 = lst[1]
                            para_2 = lst[0]
                            para_3 = all_points[-3]
                            
                            
                            para_taken = True
                            y_lim = 480
                            y_new = 455
                            x_new1,y_new1 = para_3
                            x_new2,y_new2 = para_2
                            x_new3,y_new3 = para_1


                            x1,y1=[para_3[0],y_new-para_3[1]]
                            x2,y2=[para_2[0],y_new-para_2[1]]                        
                            x3,y3=[para_1[0],y_new-para_1[1]]

                            a,b,c=calc_parabola_vertex(x1, y1, x2, y2, x3, y3)

                            coord_new1,coord_new2= solve_Quadratic(a,b,c,y_lim-y_new)
                            x4,y4 = [int(coord_new1.real),y_new]
                            x5,y5 = [int(coord_new2.real),y_new]

                        
                        print(x4,y4)
                        
                        send_gcode(grbl_board, '$X')
                        
                        if centre_x_r < x4:
                            print('cup is to left of destination')
            
                            temp1 = centre_x_r

                            while temp1 < x4:
                                if temp1 > near_step-5:
                                    break
                                send_gcode(grbl_board, 'G21 G91 G1 X{} F900'.format(step_val))
                                temp1+=1
                        
                        if centre_x_r > x4:
                            print('cup is to right of desytination')
                            temp2 = centre_x_r

                            while temp2 > x4:
                                if temp2 < far_step-5:
                                    break
                                send_gcode(grbl_board, 'G21 G91 G1 X-{} F900'.format(step_val))
                                temp2-=1
                        
                        #if centre_x_r 
                        """
                        if x>=230 and y<460:
                            dis_travel = int(x4)*0.018125
                            send_gcode(grbl_board, '$X')
                            send_gcode(grbl_board, 'G21 G91 G1 X'+ str(dis_travel) +' F400')
                        """
                        max_point = lst[0]

                        cv2.circle(frame1, (para_3[0],para_3[1]), 10, (0, 0, 255), -1)
                        cv2.circle(frame1, (para_2[0],para_2[1]), 10, (0, 255, 0), -1)
                        cv2.circle(frame1, (para_1[0],para_1[1]), 10, (255, 0, 0), -1)

                        if x_new1>x_new3:
                            maxroot = True
                            #finding angle if thrown from min to max on x axis
                            a1 = (initial_cord[0]-100, initial_cord[1])
                            a2 = (initial_cord)
                            a3 = (max_point)
                            angle = angle3pt(a3,a2,a1)
                            angle_line_point = 0
                        else:
                            #finding angle if thrown from max to min on x axis
                            a1 = (initial_cord[0]+100, initial_cord[1])
                            a2 = (initial_cord)
                            a3 = (max_point)
                            angle = angle3pt(a3,a2,a1)
                            angle_line_point = screen_width
                            
                        max_found_disp = True
                        max_found = True
                        
                if centre_y < (screen_height-15) and not draw:
                    draw = True
                    second_active = True
                    cord = [centre_x,centre_y]
                    end_time = time.time()
                
        #displays first object capture location
        if first_active:
            cv2.rectangle(frame,(initial_cord[0],initial_cord[1]),(initial_cord[0]+50,initial_cord[1]+50), (255,0, 0), 2)

        #displays second object capture location
        if second_active:
            cv2.rectangle(frame,(cord[0],cord[1]),(cord[0]+50,cord[1]+50), (0,255, 0), 2)
            cv2.line(frame, (tuple(cord)), (tuple(initial_cord)), (0, 0, 255), 2)
            cv2.line(frame, (initial_cord[0], initial_cord[1]), (angle_line_point,initial_cord[1]), (0, 0, 255), 2)

            cv2.putText(frame, 'tilt angle:'+str(round(angle,2)),(20, 20), font,0.6, (0, 0, 0), 2)#angle

            if max_found_disp:
                cv2.line(frame, (initial_cord[0], initial_cord[1]), (max_point[0],max_point[1]), (0, 0, 255), 2)

                if maxroot:
                    cv2.circle(frame, (x5,y5), 5, (0, random.randint(0,255), 255), -1)
                    cv2.circle(frame1, (x5,y5), 5, (0, random.randint(0,255), 255), -1)

                    if x5>screen_width:
                        cv2.putText(frame, 'Limit Exceeded',(20, 40), font,0.6, (255, 0, 0), 2)

                else:
                    if x4<0:
                        cv2.putText(frame, 'Limit Exceeded',(20, 40), font,0.6, (255, 0, 0), 2)

                    cv2.circle(frame, (x4,y4), 5, (0, random.randint(0,255), 255), -1)
                    cv2.circle(frame1, (x4,y4), 5, (0, random.randint(0,255), 255), -1)

                    

        if cv2.waitKey(33) & 0xFF == ord('n'):
            print('n pressed')
            # send_gcode(grbl_board, 'G21 G91 G1 X'+ str(dis_travel*-1) +' F1350')
            max_found_disp = False
            first_active = False
            second_active = False
            obj_seen = False
            lst.clear()
            draw = False
            first_taken = False
            max_found = False
            maxroot = False
            para_1.clear()
            para_2.clear()
            para_3.clear()
            para_taken = False
            
            all_points.clear()
        
        if cv2.waitKey(33) & 0xFF == ord('h'):
            print('homing')
            temp3 = centre_x_r

            while temp3 > 360:
                print(temp3) 
                send_gcode(grbl_board, 'G21 G91 G1 X0.049699570  F1350')
                temp3-=1

        if cv2.waitKey(33) & 0xFF == ord('a'):
            far_step = centre_x_r
            print("far step value taken",far_step)
            print("Screen widhth - a ",screen_width-far_step)

        if cv2.waitKey(33) & 0xFF == ord('b'):
            near_step = centre_x_r
            print("Near Step value taken")
            step_val = 11.58 / (near_step-far_step)
            print ("Stepval",step_val)

        cv2.imshow("frame1", frame1)
        cv2.imshow("frame", frame)
        #cv2.imshow("RedMask", mask_red)
        #cv2.imshow("ywllowMask", mask_yellow)

        cv2.moveWindow("frame", 0, 70)
        cv2.moveWindow("frame1", 700, 70)

        key = cv2.waitKey(1)
        if key == 27:
            break
    except Exception as e: 
        print(e)
cap.release()
cv2.destroyAllWindows()
